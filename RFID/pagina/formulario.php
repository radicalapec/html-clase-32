<html lang="es">
    <head>
        <meta charset="utf-8">

        <title>Escritura de TAGS - Radical</title>
        <meta name="description" content="html">
        <meta name="author" content="Radical">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        
        <script>
              var recibirMensaje = function recibirMensaje() {
                $.ajax({
                    type: "POST",
                    url: "respuesta.php",
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR,textStatus,errorThrown);
                    },
                    dataType: 'json',
                    success: function(algo) {
                        if(algo.respuesta =='5'){
                            $("#respuesta").html("<span class='label label-success'>¡ Escritura exitosa !</span>");
                            setTimeout(function() {
                                $('#myModal').modal('hide')
                            },1500)
                        }else{
                            $("#respuesta").html("<span class='label label-danger'>¡Debe colocar un tag !</span>");
                            recibirMensaje();
                        }
                    }        
                        
                });
            }

            var enviarMensaje = function enviarMensaje() {
                $.ajax({
                    type: "POST",
                    url: "envio.php",
                    data: { codigo: $("#codigo").val()},
            
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR,textStatus,errorThrown);
                    },
                
                    success: function(response) {
                        recibirMensaje();
                    }        

                        
                });
            }


            $(function() {
              

                $("#enviar").click(function() {
                     $("#respuesta").html("<span class='label label-primary'>Esperando Respuesta...</span>");
                    $('#myModal').modal();

                    enviarMensaje();
                });
            });    
        </script>

        <style>
          .carousel-inner > .item > img,
          .carousel-inner > .item > a > img {
              width: 100%;
              margin: auto;
          }
          </style>

        <!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body>
      <h2><center>ESCRITURA DE TAGS</center></h2>    
        <div class="panel panel-success">
          <div class="panel-heading">PRESIONA ENVIAR PARA INICIO DE ESCRITURA</div>
          <div class="panel-body">
            <form action="envio.php" method="post">
                    Codigo RFID : <input type="text" name="codigo" size="22" maxlength="24" id="codigo">
                    <button type="button" class="btn btn-info" id="enviar">Enviar</button>
                    <!-- <input type="button" value="Enviar" id="enviar"> -->

                </form>
          </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Respuesta</h4>
              </div>
              <div class="modal-body">
                <div id="respuesta"><span class="label label-primary">Esperando Respuesta...</span></div>
              </div>
            
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
      </div>


        <div class="container">
          <br>
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="img/APEC.gif" alt="Chania" width="460" height="345">
              </div>

              <div class="item">
                <img src="img/APEC2.jpg" alt="Chania" width="460" height="345">
              </div>
            
              <div class="item">
                <img src="img/APEC2.png" alt="Flower" width="460" height="345">
              </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>


    </body>
</html>