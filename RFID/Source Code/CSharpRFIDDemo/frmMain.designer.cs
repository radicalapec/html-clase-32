﻿namespace CSharpRFIDDemo
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOpenConn = new System.Windows.Forms.Button();
            this.gbxCommModel = new System.Windows.Forms.GroupBox();
            this.cbxCom = new System.Windows.Forms.ComboBox();
            this.txtPoint = new System.Windows.Forms.TextBox();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.rbnCom = new System.Windows.Forms.RadioButton();
            this.rbnTCP = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnCloseConn = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.cbxBeep = new System.Windows.Forms.CheckBox();
            this.btnReadCards = new System.Windows.Forms.Button();
            this.listViewCard = new System.Windows.Forms.ListView();
            this.colNumber = new System.Windows.Forms.ColumnHeader();
            this.colCardNumber = new System.Windows.Forms.ColumnHeader();
            this.colReadCount = new System.Windows.Forms.ColumnHeader();
            this.colBeginTime = new System.Windows.Forms.ColumnHeader();
            this.colLastTime = new System.Windows.Forms.ColumnHeader();
            this.colAntenna = new System.Windows.Forms.ColumnHeader();
            this.timerUpdateListView = new System.Windows.Forms.Timer(this.components);
            this.btnStopReadCards = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnListViewCard = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LabelDisTig = new System.Windows.Forms.Label();
            this.btnShowCopyright = new System.Windows.Forms.Button();
            this.btnSetReaderName = new System.Windows.Forms.Button();
            this.btnLoadSysInfo = new System.Windows.Forms.Button();
            this.txtFpgaVersion = new System.Windows.Forms.TextBox();
            this.txtRFHardVersion = new System.Windows.Forms.TextBox();
            this.txtBaseHardVersion = new System.Windows.Forms.TextBox();
            this.txtProcessVersion = new System.Windows.Forms.TextBox();
            this.txtReaderSerial = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtReaderType = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtReaderName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lstResult = new System.Windows.Forms.ListBox();
            this.btnClearResult = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnIOOperate = new System.Windows.Forms.Button();
            this.cbxState = new System.Windows.Forms.ComboBox();
            this.cbxPort = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCardCode = new System.Windows.Forms.TextBox();
            this.btnWriteCard = new System.Windows.Forms.Button();
            this.btn_prueba = new System.Windows.Forms.Button();
            this.gbxCommModel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOpenConn
            // 
            this.btnOpenConn.Location = new System.Drawing.Point(12, 165);
            this.btnOpenConn.Name = "btnOpenConn";
            this.btnOpenConn.Size = new System.Drawing.Size(96, 25);
            this.btnOpenConn.TabIndex = 0;
            this.btnOpenConn.Text = "Connect";
            this.btnOpenConn.UseVisualStyleBackColor = true;
            this.btnOpenConn.Click += new System.EventHandler(this.btnOpenConn_Click);
            // 
            // gbxCommModel
            // 
            this.gbxCommModel.Controls.Add(this.cbxCom);
            this.gbxCommModel.Controls.Add(this.txtPoint);
            this.gbxCommModel.Controls.Add(this.txtIP);
            this.gbxCommModel.Controls.Add(this.rbnCom);
            this.gbxCommModel.Controls.Add(this.rbnTCP);
            this.gbxCommModel.Controls.Add(this.label11);
            this.gbxCommModel.Controls.Add(this.label10);
            this.gbxCommModel.Location = new System.Drawing.Point(12, 9);
            this.gbxCommModel.Name = "gbxCommModel";
            this.gbxCommModel.Size = new System.Drawing.Size(152, 150);
            this.gbxCommModel.TabIndex = 1;
            this.gbxCommModel.TabStop = false;
            this.gbxCommModel.Text = "Connect type";
            // 
            // cbxCom
            // 
            this.cbxCom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCom.FormattingEnabled = true;
            this.cbxCom.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10"});
            this.cbxCom.Location = new System.Drawing.Point(43, 118);
            this.cbxCom.Name = "cbxCom";
            this.cbxCom.Size = new System.Drawing.Size(102, 21);
            this.cbxCom.TabIndex = 7;
            // 
            // txtPoint
            // 
            this.txtPoint.Location = new System.Drawing.Point(43, 69);
            this.txtPoint.Name = "txtPoint";
            this.txtPoint.Size = new System.Drawing.Size(102, 20);
            this.txtPoint.TabIndex = 4;
            this.txtPoint.TextChanged += new System.EventHandler(this.txtPoint_TextChanged);
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(43, 42);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(102, 20);
            this.txtIP.TabIndex = 4;
            this.txtIP.TextChanged += new System.EventHandler(this.txtIP_TextChanged);
            // 
            // rbnCom
            // 
            this.rbnCom.AutoSize = true;
            this.rbnCom.Location = new System.Drawing.Point(10, 99);
            this.rbnCom.Name = "rbnCom";
            this.rbnCom.Size = new System.Drawing.Size(49, 17);
            this.rbnCom.TabIndex = 1;
            this.rbnCom.TabStop = true;
            this.rbnCom.Text = "COM";
            this.rbnCom.UseVisualStyleBackColor = true;
            // 
            // rbnTCP
            // 
            this.rbnTCP.AutoSize = true;
            this.rbnTCP.Location = new System.Drawing.Point(10, 25);
            this.rbnTCP.Name = "rbnTCP";
            this.rbnTCP.Size = new System.Drawing.Size(42, 17);
            this.rbnTCP.TabIndex = 1;
            this.rbnTCP.TabStop = true;
            this.rbnTCP.Text = "Net";
            this.rbnTCP.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Port:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "IP:";
            // 
            // btnCloseConn
            // 
            this.btnCloseConn.Location = new System.Drawing.Point(12, 196);
            this.btnCloseConn.Name = "btnCloseConn";
            this.btnCloseConn.Size = new System.Drawing.Size(96, 25);
            this.btnCloseConn.TabIndex = 0;
            this.btnCloseConn.Text = "Disconnect";
            this.btnCloseConn.UseVisualStyleBackColor = true;
            this.btnCloseConn.Click += new System.EventHandler(this.btnCloseConn_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(12, 529);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(96, 25);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cbxBeep
            // 
            this.cbxBeep.AutoSize = true;
            this.cbxBeep.Location = new System.Drawing.Point(12, 239);
            this.cbxBeep.Name = "cbxBeep";
            this.cbxBeep.Size = new System.Drawing.Size(51, 17);
            this.cbxBeep.TabIndex = 2;
            this.cbxBeep.Text = "Beep";
            this.cbxBeep.UseVisualStyleBackColor = true;
            this.cbxBeep.CheckedChanged += new System.EventHandler(this.cbxBeep_CheckedChanged);
            // 
            // btnReadCards
            // 
            this.btnReadCards.Location = new System.Drawing.Point(45, 22);
            this.btnReadCards.Name = "btnReadCards";
            this.btnReadCards.Size = new System.Drawing.Size(75, 25);
            this.btnReadCards.TabIndex = 3;
            this.btnReadCards.Text = "Start";
            this.btnReadCards.UseVisualStyleBackColor = true;
            this.btnReadCards.Click += new System.EventHandler(this.btnReadCards_Click);
            // 
            // listViewCard
            // 
            this.listViewCard.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNumber,
            this.colCardNumber,
            this.colReadCount,
            this.colBeginTime,
            this.colLastTime,
            this.colAntenna});
            this.listViewCard.FullRowSelect = true;
            this.listViewCard.Location = new System.Drawing.Point(6, 53);
            this.listViewCard.Name = "listViewCard";
            this.listViewCard.Size = new System.Drawing.Size(625, 122);
            this.listViewCard.TabIndex = 4;
            this.listViewCard.UseCompatibleStateImageBehavior = false;
            this.listViewCard.View = System.Windows.Forms.View.Details;
            // 
            // colNumber
            // 
            this.colNumber.Text = "No";
            // 
            // colCardNumber
            // 
            this.colCardNumber.Text = "Tag Number";
            this.colCardNumber.Width = 200;
            // 
            // colReadCount
            // 
            this.colReadCount.Text = "frequency";
            this.colReadCount.Width = 80;
            // 
            // colBeginTime
            // 
            this.colBeginTime.Text = "Start Time";
            this.colBeginTime.Width = 80;
            // 
            // colLastTime
            // 
            this.colLastTime.Text = "Current Time";
            this.colLastTime.Width = 87;
            // 
            // colAntenna
            // 
            this.colAntenna.Text = "Antenna";
            this.colAntenna.Width = 116;
            // 
            // timerUpdateListView
            // 
            this.timerUpdateListView.Enabled = true;
            this.timerUpdateListView.Interval = 200;
            this.timerUpdateListView.Tick += new System.EventHandler(this.timerUpdateListView_Tick);
            // 
            // btnStopReadCards
            // 
            this.btnStopReadCards.Location = new System.Drawing.Point(126, 22);
            this.btnStopReadCards.Name = "btnStopReadCards";
            this.btnStopReadCards.Size = new System.Drawing.Size(75, 25);
            this.btnStopReadCards.TabIndex = 3;
            this.btnStopReadCards.Text = "Stop";
            this.btnStopReadCards.UseVisualStyleBackColor = true;
            this.btnStopReadCards.Click += new System.EventHandler(this.btnStopReadCards_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnListViewCard);
            this.groupBox1.Controls.Add(this.btnReadCards);
            this.groupBox1.Controls.Add(this.btnStopReadCards);
            this.groupBox1.Controls.Add(this.listViewCard);
            this.groupBox1.Location = new System.Drawing.Point(170, 310);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(644, 180);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Read Tag";
            // 
            // btnListViewCard
            // 
            this.btnListViewCard.Location = new System.Drawing.Point(246, 23);
            this.btnListViewCard.Name = "btnListViewCard";
            this.btnListViewCard.Size = new System.Drawing.Size(75, 25);
            this.btnListViewCard.TabIndex = 5;
            this.btnListViewCard.Text = "Clear";
            this.btnListViewCard.UseVisualStyleBackColor = true;
            this.btnListViewCard.Click += new System.EventHandler(this.btnListViewCard_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LabelDisTig);
            this.groupBox2.Controls.Add(this.btnShowCopyright);
            this.groupBox2.Controls.Add(this.btnSetReaderName);
            this.groupBox2.Controls.Add(this.btnLoadSysInfo);
            this.groupBox2.Controls.Add(this.txtFpgaVersion);
            this.groupBox2.Controls.Add(this.txtRFHardVersion);
            this.groupBox2.Controls.Add(this.txtBaseHardVersion);
            this.groupBox2.Controls.Add(this.txtProcessVersion);
            this.groupBox2.Controls.Add(this.txtReaderSerial);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtReaderType);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtReaderName);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(170, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(644, 295);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "System Information";
            // 
            // LabelDisTig
            // 
            this.LabelDisTig.BackColor = System.Drawing.Color.Transparent;
            this.LabelDisTig.Font = new System.Drawing.Font("SimSun", 20F);
            this.LabelDisTig.ForeColor = System.Drawing.Color.Red;
            this.LabelDisTig.Location = new System.Drawing.Point(251, 13);
            this.LabelDisTig.Name = "LabelDisTig";
            this.LabelDisTig.Size = new System.Drawing.Size(388, 35);
            this.LabelDisTig.TabIndex = 6;
            // 
            // btnShowCopyright
            // 
            this.btnShowCopyright.Location = new System.Drawing.Point(128, 22);
            this.btnShowCopyright.Name = "btnShowCopyright";
            this.btnShowCopyright.Size = new System.Drawing.Size(119, 25);
            this.btnShowCopyright.TabIndex = 5;
            this.btnShowCopyright.Text = "Copy Right";
            this.btnShowCopyright.UseVisualStyleBackColor = true;
            this.btnShowCopyright.Click += new System.EventHandler(this.btnShowCopyright_Click);
            // 
            // btnSetReaderName
            // 
            this.btnSetReaderName.Location = new System.Drawing.Point(556, 56);
            this.btnSetReaderName.Name = "btnSetReaderName";
            this.btnSetReaderName.Size = new System.Drawing.Size(75, 22);
            this.btnSetReaderName.TabIndex = 3;
            this.btnSetReaderName.Text = "Set";
            this.btnSetReaderName.UseVisualStyleBackColor = true;
            this.btnSetReaderName.Click += new System.EventHandler(this.btnSetReaderName_Click);
            // 
            // btnLoadSysInfo
            // 
            this.btnLoadSysInfo.Location = new System.Drawing.Point(3, 22);
            this.btnLoadSysInfo.Name = "btnLoadSysInfo";
            this.btnLoadSysInfo.Size = new System.Drawing.Size(119, 25);
            this.btnLoadSysInfo.TabIndex = 2;
            this.btnLoadSysInfo.Text = "SystemInformation";
            this.btnLoadSysInfo.UseVisualStyleBackColor = true;
            this.btnLoadSysInfo.Click += new System.EventHandler(this.btnLoadSysInfo_Click);
            // 
            // txtFpgaVersion
            // 
            this.txtFpgaVersion.Location = new System.Drawing.Point(189, 173);
            this.txtFpgaVersion.Name = "txtFpgaVersion";
            this.txtFpgaVersion.ReadOnly = true;
            this.txtFpgaVersion.Size = new System.Drawing.Size(340, 20);
            this.txtFpgaVersion.TabIndex = 1;
            // 
            // txtRFHardVersion
            // 
            this.txtRFHardVersion.Location = new System.Drawing.Point(189, 232);
            this.txtRFHardVersion.Name = "txtRFHardVersion";
            this.txtRFHardVersion.ReadOnly = true;
            this.txtRFHardVersion.Size = new System.Drawing.Size(340, 20);
            this.txtRFHardVersion.TabIndex = 1;
            // 
            // txtBaseHardVersion
            // 
            this.txtBaseHardVersion.Location = new System.Drawing.Point(189, 203);
            this.txtBaseHardVersion.Name = "txtBaseHardVersion";
            this.txtBaseHardVersion.ReadOnly = true;
            this.txtBaseHardVersion.Size = new System.Drawing.Size(340, 20);
            this.txtBaseHardVersion.TabIndex = 1;
            // 
            // txtProcessVersion
            // 
            this.txtProcessVersion.Location = new System.Drawing.Point(189, 144);
            this.txtProcessVersion.Name = "txtProcessVersion";
            this.txtProcessVersion.ReadOnly = true;
            this.txtProcessVersion.Size = new System.Drawing.Size(340, 20);
            this.txtProcessVersion.TabIndex = 1;
            // 
            // txtReaderSerial
            // 
            this.txtReaderSerial.Location = new System.Drawing.Point(189, 86);
            this.txtReaderSerial.MaxLength = 8;
            this.txtReaderSerial.Name = "txtReaderSerial";
            this.txtReaderSerial.ReadOnly = true;
            this.txtReaderSerial.Size = new System.Drawing.Size(340, 20);
            this.txtReaderSerial.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "RF hardware version:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "FPGA/DSP Software Version:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Baseband hardware Version:";
            // 
            // txtReaderType
            // 
            this.txtReaderType.Location = new System.Drawing.Point(189, 115);
            this.txtReaderType.MaxLength = 5;
            this.txtReaderType.Name = "txtReaderType";
            this.txtReaderType.ReadOnly = true;
            this.txtReaderType.Size = new System.Drawing.Size(340, 20);
            this.txtReaderType.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Processor Software Version:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Reader Model:";
            // 
            // txtReaderName
            // 
            this.txtReaderName.Location = new System.Drawing.Point(189, 56);
            this.txtReaderName.MaxLength = 8;
            this.txtReaderName.Name = "txtReaderName";
            this.txtReaderName.Size = new System.Drawing.Size(340, 20);
            this.txtReaderName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Product serial number:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Reader Name:";
            // 
            // lstResult
            // 
            this.lstResult.FormattingEnabled = true;
            this.lstResult.Location = new System.Drawing.Point(12, 363);
            this.lstResult.Name = "lstResult";
            this.lstResult.Size = new System.Drawing.Size(152, 160);
            this.lstResult.TabIndex = 8;
            // 
            // btnClearResult
            // 
            this.btnClearResult.Location = new System.Drawing.Point(12, 333);
            this.btnClearResult.Name = "btnClearResult";
            this.btnClearResult.Size = new System.Drawing.Size(96, 25);
            this.btnClearResult.TabIndex = 9;
            this.btnClearResult.Text = "Clear Result";
            this.btnClearResult.UseVisualStyleBackColor = true;
            this.btnClearResult.Click += new System.EventHandler(this.btnClearResult_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnIOOperate);
            this.groupBox3.Controls.Add(this.cbxState);
            this.groupBox3.Controls.Add(this.cbxPort);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtCardCode);
            this.groupBox3.Controls.Add(this.btnWriteCard);
            this.groupBox3.Location = new System.Drawing.Point(171, 497);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(643, 56);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Write Tag";
            // 
            // btnIOOperate
            // 
            this.btnIOOperate.Location = new System.Drawing.Point(487, 17);
            this.btnIOOperate.Name = "btnIOOperate";
            this.btnIOOperate.Size = new System.Drawing.Size(91, 25);
            this.btnIOOperate.TabIndex = 36;
            this.btnIOOperate.Text = "IO Output";
            this.btnIOOperate.UseVisualStyleBackColor = true;
            this.btnIOOperate.Click += new System.EventHandler(this.btnIOOperate_Click);
            // 
            // cbxState
            // 
            this.cbxState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxState.FormattingEnabled = true;
            this.cbxState.Items.AddRange(new object[] {
            "Output Low Voltage",
            "Output High Voltage",
            "Output Positive Pulse",
            "Output Negative Pulse"});
            this.cbxState.Location = new System.Drawing.Point(415, 20);
            this.cbxState.Name = "cbxState";
            this.cbxState.Size = new System.Drawing.Size(66, 21);
            this.cbxState.TabIndex = 35;
            // 
            // cbxPort
            // 
            this.cbxPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPort.FormattingEnabled = true;
            this.cbxPort.Items.AddRange(new object[] {
            "All",
            "1",
            "2",
            "3",
            "4"});
            this.cbxPort.Location = new System.Drawing.Point(343, 21);
            this.cbxPort.Name = "cbxPort";
            this.cbxPort.Size = new System.Drawing.Size(66, 21);
            this.cbxPort.TabIndex = 35;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(255, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "IO OutPut:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 34;
            this.label9.Text = "EPC：";
            // 
            // txtCardCode
            // 
            this.txtCardCode.Location = new System.Drawing.Point(53, 20);
            this.txtCardCode.MaxLength = 24;
            this.txtCardCode.Name = "txtCardCode";
            this.txtCardCode.Size = new System.Drawing.Size(132, 20);
            this.txtCardCode.TabIndex = 33;
            this.txtCardCode.TextChanged += new System.EventHandler(this.txtCardCode_TextChanged);
            // 
            // btnWriteCard
            // 
            this.btnWriteCard.Location = new System.Drawing.Point(191, 20);
            this.btnWriteCard.Name = "btnWriteCard";
            this.btnWriteCard.Size = new System.Drawing.Size(52, 25);
            this.btnWriteCard.TabIndex = 32;
            this.btnWriteCard.Text = "Write";
            this.btnWriteCard.UseVisualStyleBackColor = true;
            this.btnWriteCard.Click += new System.EventHandler(this.btnWriteCard_Click);
            // 
            // btn_prueba
            // 
            this.btn_prueba.Location = new System.Drawing.Point(114, 165);
            this.btn_prueba.Name = "btn_prueba";
            this.btn_prueba.Size = new System.Drawing.Size(50, 25);
            this.btn_prueba.TabIndex = 11;
            this.btn_prueba.Text = "prueba";
            this.btn_prueba.UseVisualStyleBackColor = true;
            this.btn_prueba.Click += new System.EventHandler(this.btn_prueba_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 561);
            this.Controls.Add(this.btn_prueba);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnClearResult);
            this.Controls.Add(this.lstResult);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbxBeep);
            this.Controls.Add(this.gbxCommModel);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnCloseConn);
            this.Controls.Add(this.btnOpenConn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "C# FRID Demo";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.gbxCommModel.ResumeLayout(false);
            this.gbxCommModel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpenConn;
        private System.Windows.Forms.GroupBox gbxCommModel;
        private System.Windows.Forms.RadioButton rbnTCP;
        private System.Windows.Forms.Button btnCloseConn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.CheckBox cbxBeep;
        private System.Windows.Forms.Button btnReadCards;
        private System.Windows.Forms.ListView listViewCard;
        public System.Windows.Forms.ColumnHeader colNumber;
        public System.Windows.Forms.ColumnHeader colCardNumber;
        public System.Windows.Forms.ColumnHeader colReadCount;
        public System.Windows.Forms.ColumnHeader colBeginTime;
        public System.Windows.Forms.ColumnHeader colLastTime;
        public System.Windows.Forms.ColumnHeader colAntenna;
        private System.Windows.Forms.Timer timerUpdateListView;
        private System.Windows.Forms.Button btnStopReadCards;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtReaderName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtReaderSerial;
        private System.Windows.Forms.TextBox txtReaderType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProcessVersion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFpgaVersion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBaseHardVersion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRFHardVersion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnLoadSysInfo;
        private System.Windows.Forms.Button btnSetReaderName;
        private System.Windows.Forms.Button btnShowCopyright;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPoint;
        private System.Windows.Forms.ListBox lstResult;
        private System.Windows.Forms.Button btnListViewCard;
        private System.Windows.Forms.Button btnClearResult;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCardCode;
        private System.Windows.Forms.Button btnWriteCard;
        private System.Windows.Forms.ComboBox cbxCom;
        private System.Windows.Forms.RadioButton rbnCom;
        private System.Windows.Forms.ComboBox cbxPort;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbxState;
        private System.Windows.Forms.Button btnIOOperate;
        private System.Windows.Forms.Label LabelDisTig;
        private System.Windows.Forms.Button btn_prueba;
    }
}

