using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpRFIDDemo
{
    /// <summary>
    /// 数据信息结构体,用于存放1个卡数据信息
    /// </summary>
    public struct RevMsgStruct
    {
        /// <summary>
        /// 卡数据
        /// </summary>
        public string sCodeData;

        /// <summary>
        /// 天线号
        /// </summary>
        public int nAntenna;

        /// <summary>
        /// 开始读取时间
        /// </summary>
        public DateTime tBeginTime;
        
        /// <summary>
        /// 结束读取时间
        /// </summary>
        public DateTime tLastTime;
        
        /// <summary>
        /// 读取次数
        /// </summary>
        public long nRepeatTime;
    }


    /// <summary>
    /// RevMsgBuffer是用Dictionary来存放所有的卡信息
    /// </summary>
    class RevMsgBuffer
    {
        List<RevMsgStruct> revMsgArray = new List<RevMsgStruct>();

        /// <summary>
        /// 添加新卡信息，如有重复的则直接更新
        /// </summary>
        /// <param name="revMsg"></param>
        public void RevMsgAdd(RevMsgStruct revMsg)
        {
            for (int tempi = 0; tempi < revMsgArray.Count; ++tempi)
            {
                if (revMsgArray[tempi].sCodeData == revMsg.sCodeData && revMsgArray[tempi].nAntenna == revMsg.nAntenna)
                {
                    RevMsgStruct revMsgTemp = revMsgArray[tempi];
                    revMsgTemp.nRepeatTime += revMsg.nRepeatTime;
                    revMsgTemp.tLastTime = revMsg.tLastTime;
                    revMsgArray[tempi] = revMsgTemp;
                    return;
                }
            }
            revMsgArray.Add(revMsg);
        }

        /// <summary>
        /// 移除某一条信息
        /// </summary>
        /// <param name="RemoveCodeData"></param>
        public void RemoveMsg(string RemoveCodeData)
        {
            int nRemoveNum = 0;
            while (nRemoveNum == revMsgArray.Count)
            {
                if (revMsgArray[nRemoveNum].sCodeData == RemoveCodeData)
                    revMsgArray.Remove(revMsgArray[nRemoveNum]);
            }
        }

        /// <summary>
        /// 清空
        /// </summary>
        public void ClearMsg()
        {
            revMsgArray.Clear();
        }

        /// <summary>
        /// 在获取的卡信息包队列中,返回最后的1条卡信息
        /// </summary>
        /// <param name="revMsg"></param>
        public bool RevMsgGet(ref RevMsgStruct revMsg, int nArrayNum)
        {
            if (revMsgArray.Count == 0)
                return false;

            if (nArrayNum >= 0 && nArrayNum < revMsgArray.Count)
            {
                revMsg = revMsgArray[nArrayNum];
                return true;
            }
            return false;
        }

        public RevMsgStruct Get(int i)
        {
            return revMsgArray[i];
        }

        public int TagCount
        {
            get { return revMsgArray.Count; }
        }
    }
}
