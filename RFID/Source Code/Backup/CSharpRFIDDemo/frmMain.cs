using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using Szaat.RFID.CSharpAPI;
using System.Runtime.InteropServices;

namespace CSharpRFIDDemo
{
    public partial class frmMain : Form
    {

        #region Parameters

        public string sReaderIP = "192.168.0.238";

        public int iReaderPoint = 7086;

        /// <summary>
        /// Connect Status
        /// </summary>
        public bool bConnState = false;

        /// <summary>
        /// Port Handle
        /// </summary>
        public IntPtr pHandle = IntPtr.Zero;

        /// <summary>
        /// Read Tag Thread
        /// </summary>
        Thread readThread = null;

        /// <summary>
        /// Read Tag Status
        /// </summary>
        bool bReadCodeState = false;


        /// <summary>
        /// Tag Information Buffer
        /// </summary>
        RevMsgBuffer revMsgLine = new RevMsgBuffer();

        /// <summary>
        /// Operating type
        /// </summary>
        int SAAT_CODE_TYPE = 0;

        /// <summary>
        /// Antenna No.
        /// </summary>
        byte sAntenna = 1;

        /// <summary>
        /// Read Tag Type
        /// </summary>
        int SAAT_READ_TYPE = 1;

        /// <summary>
        /// Beep
        /// </summary>
        public bool bBeep = true;


        /// <summary>
        /// System information Array
        /// </summary>
        public static string[] sSysInfoBuf = new string[7];
        /// <summary>
        /// 
        /// System information Character length
        /// </summary>
        public static byte[] sSysInfoLen = new byte[] { 8, 6, 8, 4, 4, 4, 4 };

        #endregion

        #region 

        public frmMain()
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        /// <summary>
        /// Initializing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;

            txtIP.Text = sReaderIP;
            txtPoint.Text = iReaderPoint.ToString();
            cbxBeep.Checked = bBeep;
            rbnTCP.Checked = true;

            upButtonEnabled();
        }

        /// <summary>
        /// Beep
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxBeep_CheckedChanged(object sender, EventArgs e)
        {
            bBeep = cbxBeep.Checked;
        }

        /// <summary>
        /// Change Ip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIP_TextChanged(object sender, EventArgs e)
        {
            string _ip = txtIP.Text;
            if (!IsCorrenctIP(_ip)) { ShowResult("Incorrect IP address configuration!", Win32.BeepType.Failure); txtIP.Text = sReaderIP; return; }

            sReaderIP = txtIP.Text;
        }

        /// <summary>
        /// Check the IP address is valid
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public bool IsCorrenctIP(string ip)
        {
            string pattrn = @"(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])";
            if (System.Text.RegularExpressions.Regex.IsMatch(ip, pattrn))
            {
                return true;
            }
            else
            {
                return false;

            }
        }

        /// <summary>
        /// Change Port
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPoint_TextChanged(object sender, EventArgs e)
        {
            int _point;
            try
            {
                _point = int.Parse(txtPoint.Text);
                if (_point > 65535) { throw new Exception(); }
                iReaderPoint = _point;
            }
            catch { ShowResult("Incorrect Port configuration", Win32.BeepType.Failure); txtPoint.Text = iReaderPoint.ToString(); return; }
        }


        /// <summary>
        /// Display record, sound indicate
        /// </summary>
        /// <param name="_msg"></param>
        /// <param name="_beepType"></param>
        private void ShowResult(string _msg, Win32.BeepType _beepType)
        {
            lstResult.Items.Add(_msg);
            lstResult.SelectedIndex = lstResult.Items.Count - 1;

            ShowResult(_beepType);
        }

        /// <summary>
        /// Beep
        /// </summary>
        /// <param name="_beepType">Beep Type</param>
        private void ShowResult(Win32.BeepType _beepType)
        {
            if (bBeep) { Win32.Beep(_beepType); }
        }

        /// <summary>
        /// Update button status
        /// </summary>
        private void upButtonEnabled()
        {

            btnOpenConn.Enabled = !bConnState;
            rbnTCP.Enabled = !bConnState;
            txtIP.Enabled = !bConnState;
            txtPoint.Enabled = !bConnState;

            btnCloseConn.Enabled = bConnState;

            btnReadCards.Enabled = !bReadCodeState && bConnState; 
            btnStopReadCards.Enabled = bReadCodeState && bConnState;

            btnLoadSysInfo.Enabled = !bReadCodeState && bConnState;
            btnSetReaderName.Enabled = !bReadCodeState && bConnState;
            btnShowCopyright.Enabled = !bReadCodeState && bConnState; 
        }

        /// <summary>
        /// Reset Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnListViewCard_Click(object sender, EventArgs e)
        {
            revMsgLine.ClearMsg();
            listViewCard.Items.Clear();
        }

        /// <summary>
        /// Clear Result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearResult_Click(object sender, EventArgs e)
        {
            lstResult.Items.Clear();
        }

        #endregion


        #region 
        /// <summary>
        /// Open Connect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpenConn_Click(object sender, EventArgs e)
        {

            bool bInit = false;

            if (rbnTCP.Checked)
            {
                //1.TCP Parameter Initialization
                bInit = RfidApi.SAAT_TCPInit(out pHandle, sReaderIP, iReaderPoint);
            }
            else
            {
                if (cbxCom.SelectedIndex < 0) 
                {
                    ShowResult("Please Select Serial Port", Win32.BeepType.Failure);
                    return;
                }

                //1.COM Parameter Initialization
                bInit = RfidApi.SAAT_COMInit(out pHandle, 0x00, cbxCom.SelectedItem.ToString(), 19200);
            }

            if (!bInit) 
            {
                ShowResult("Failed to initialized!", Win32.BeepType.Failure); 
                return; 
            }

            // 2.Connect
            bConnState = RfidApi.SAAT_Open(pHandle);
            if (!bConnState) 
            {
                ShowResult("Failed to connect!", Win32.BeepType.Failure);
                return; 
            }

            ShowResult("Succeeded to connect", Win32.BeepType.Success);
            upButtonEnabled();

        }

        private bool CloseConn()
        {
            if (pHandle == IntPtr.Zero )
            {
                return true;
            }

            bReadCodeState = false;
            bool bRet = RfidApi.SAAT_Close(pHandle);
            if (bRet)
            {
                pHandle = IntPtr.Zero;
            }
            return bRet;
        }
        /// <sumCmary>
        /// Close connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCloseConn_Click(object sender, EventArgs e)
        {
            bConnState = !CloseConn();
            if (bConnState) 
            {
                ShowResult("Failed to disconnect!", Win32.BeepType.Failure); 
                return; 
            }

            ShowResult("succeeded to disconnect!", Win32.BeepType.Success);
            upButtonEnabled();
        }

        #endregion


        #region Read Tag Function
        /// <summary>
        /// Start Reading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReadCards_Click(object sender, EventArgs e)
        {
            // Setup Timer to Update the received information
            if (readThread != null )
            {
                if (readThread.IsAlive)
                {
                    readThread.Abort();
                    readThread.Join();
                }
            }
            

            if( !CReadCode() ) 
            {
                ShowResult("Failed to send read command!", Win32.BeepType.Success);
                return;
            }

            bReadCodeState = true;
            timerUpdateListView.Enabled = true;
            revMsgLine.ClearMsg();

            ShowResult("Start Reading!", Win32.BeepType.Success);
            upButtonEnabled();

            readThread = new Thread(new ThreadStart(RevCodeMsgThread));
            readThread.Start();
        }

        
        /// <summary>
        /// Reconnect
        /// </summary>
        void Reconnect()
        {
            while( true )
            {

                CloseConn();//Disconnect
                bool bInit = false;
                if( rbnTCP.Checked )
                {
                    // Initialize
                    bInit = RfidApi.SAAT_TCPInit(out pHandle, sReaderIP, iReaderPoint);
                }
                else
                {
                    bInit = RfidApi.SAAT_COMInit(out pHandle, 0x00, cbxCom.SelectedItem.ToString(), 19200);
                }

                if( !bInit )
                {
                    continue;
                }
                

                 // Connect
                bConnState = RfidApi.SAAT_Open(pHandle);
                if( !bConnState ) 
                {
                    continue;
                }

                bReadCodeState = true;
                if( !CReadCode() )
                {
                    continue;
                }

                break;
            }
        }

        /// <summary>
        /// Thread of Receive tag information 
        /// </summary>
        private void RevCodeMsgThread()
        {
            int nRevMsgResult = 0;

            RevMsgStruct revMsg = new RevMsgStruct();
            int dwStart = System.Environment.TickCount;//Start Time

            while (bReadCodeState)
            {
                nRevMsgResult = CRevCodeMsg(ref revMsg);
                if (nRevMsgResult == 1)//Get tag information
                {
                    revMsgLine.RevMsgAdd(revMsg);
                    ShowResult(Win32.BeepType.Success);
                }

                if (System.Environment.TickCount - dwStart > 4000) //Detect every 4 seconds
                {
                    if (!RfidApi.SAAT_HeartSend(pHandle)) //Disconnect
                    {
                        LabelDisTig.Text = "The connection with reader is cut off, reconnecting now ......";
                        Reconnect();
                        LabelDisTig.Text = "";
                    }
              
                    dwStart = System.Environment.TickCount;//Update start time
                }



            }
        }

        /// <summary>
        /// Send read-tag command function
        /// </summary>
        /// <returns></returns>
        public bool CReadCode()
        {
            byte SAAT_SCAN_TRIES = 3;

            switch (SAAT_CODE_TYPE)
            {
                case 0://Read EPC Code
                    return RfidApi.SAAT_6CReadEPCCode(pHandle, sAntenna, (byte)SAAT_READ_TYPE, SAAT_SCAN_TRIES);
                case 1://Read TID Code
                    return RfidApi.SAAT_6CReadTIDCode(pHandle, sAntenna, (byte)SAAT_READ_TYPE, SAAT_SCAN_TRIES);
                case 2://Read UID Code
                    return RfidApi.SAAT_6BReadUIDCode(pHandle, sAntenna, (byte)SAAT_READ_TYPE);
            }
            return false;
        }

        /// <summary>
        /// Receive tag information function
        /// </summary>
        /// <param name="revMsg">Struct for storing tag information</param>
        /// <returns></returns>
        public int CRevCodeMsg(ref RevMsgStruct revMsg)
        {
            byte sLength = 255;     // 
            string sData = "";
            byte[] btData = new byte[255];
            int bResult = 0;

            try
            {
                switch (SAAT_CODE_TYPE)
                {
                    case 0:
                        bResult = RfidApi.SAAT_6CRevEPCMsg(pHandle, out sAntenna, btData, out sLength);
                        break;
                    case 1:
                        bResult = RfidApi.SAAT_6CRevTIDMsg(pHandle, out sAntenna, btData, out sLength);
                        break;
                    case 2:
                        bResult = RfidApi.SAAT_6BRevUIDMsg(pHandle, out sAntenna, btData, out sLength);
                        break;
                }
            }
            catch 
            { 

            }

            if (bResult == 1 && sLength != 255)
            {
                revMsg.nAntenna = (int)sAntenna;
                for (byte bytei = 0; bytei < sLength; bytei++)
                    sData += btData[bytei].ToString("X2");
                revMsg.sCodeData = sData;
                revMsg.nRepeatTime = 1;
                revMsg.tBeginTime = System.DateTime.Now;
                revMsg.tLastTime = System.DateTime.Now;
            }
            return bResult;
        }

        /// <summary>
        /// Update ListView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerUpdateListView_Tick(object sender, EventArgs e)
        {

            for (int i = 0; i < revMsgLine.TagCount; i++)
            {
                if (i < listViewCard.Items.Count)
                {
                    listViewCard.Items[i].SubItems[2].Text = revMsgLine.Get(i).nRepeatTime.ToString();
                    listViewCard.Items[i].SubItems[4].Text = revMsgLine.Get(i).tLastTime.ToString("HH:mm:ss");
                }
                else
                {
                    listViewCard.Items.Add(i.ToString());
                    listViewCard.Items[i].SubItems.Add(revMsgLine.Get(i).sCodeData.ToString());
                    listViewCard.Items[i].SubItems.Add(revMsgLine.Get(i).nRepeatTime.ToString());
                    listViewCard.Items[i].SubItems.Add(revMsgLine.Get(i).tBeginTime.ToString("HH:mm:ss"));
                    listViewCard.Items[i].SubItems.Add(revMsgLine.Get(i).tLastTime.ToString("HH:mm:ss"));
                    listViewCard.Items[i].SubItems.Add(revMsgLine.Get(i).nAntenna.ToString());
                }
            }
        }

        
        /// <summary>
        /// Stop Read Tag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStopReadCards_Click(object sender, EventArgs e)
        {
            // Stop Timer
            timerUpdateListView.Enabled = false;

            //Terminate Thread
            if (readThread.IsAlive)
            {
                readThread.Abort();
                readThread.Join();
            }

            // Stop Read Tag
            if (RfidApi.SAAT_PowerOff(pHandle))
            {
                ShowResult("Stop Read Tag!", Win32.BeepType.Success);
            }
            else
            {
                ShowResult("Failed to Stop Read Tag!" + CGetErrorMessage(), Win32.BeepType.Failure);
            }

            // Clear
            bReadCodeState = false;
            upButtonEnabled();
        }

        /// <summary>
        /// Get Error Message
        /// </summary>
        /// <returns></returns>
        public string CGetErrorMessage()
        {
            byte[] btErrorMsg = new byte[255];
            string sErrorMsg = "";
            if (pHandle == IntPtr.Zero)
                sErrorMsg = "Connection hasn't been initialized";
            else
            {
                RfidApi.SAAT_GetErrorMessage(pHandle, btErrorMsg, 255);
                sErrorMsg = Encoding.Default.GetString(btErrorMsg);
            }
            return "Error information: " + sErrorMsg;
        }

        #endregion


        #region 
        /// <summary>
        /// Exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Exit();
        }

        /// <summary>
        /// Exit The System
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            Exit();
        }

        /// <summary>
        /// 
        /// </summary>
        private void Exit()
        {
            try
            {
                if (readThread.IsAlive) readThread.Abort();
            }
            catch { }

            System.Diagnostics.Process[] LocalPro = System.Diagnostics.Process.GetProcessesByName("ProcessName");
            if (LocalPro.Length > 0)
            {
                foreach (System.Diagnostics.Process a in LocalPro) { a.Kill(); }
            }
            Application.Exit();
        }

        #endregion


        #region System Information
        private void btnLoadSysInfo_Click(object sender, EventArgs e)
        {

            string sCallback = "";

            for (byte i = 0; i <= 6; i++)
            {
                if (!GetSysInfo(i, ref sCallback, sSysInfoLen[i])) 
                {
                    ShowResult(" Failed to get system information!", Win32.BeepType.Failure);
                    return;
                }
                sSysInfoBuf[i] = sCallback;
            }

            ShowResult("Succeeded to get system information!", Win32.BeepType.Success);

            txtReaderName.Text = sSysInfoBuf[0];
            txtReaderType.Text = sSysInfoBuf[1];
            txtReaderSerial.Text = sSysInfoBuf[2];
            txtProcessVersion.Text = sSysInfoBuf[3];
            txtFpgaVersion.Text = sSysInfoBuf[4];
            txtBaseHardVersion.Text = sSysInfoBuf[5];
            txtRFHardVersion.Text = sSysInfoBuf[6];

        }

        /// <summary>
        /// Get the total system inforamtion
        /// </summary>
        /// <param name="nType"></param>
        /// <param name="sCallback"></param>
        /// <param name="iLenght"></param>
        /// <returns></returns>
        public bool GetSysInfo(byte nType, ref string sCallback, byte iLenght)
        {
            if (CSysQuery(nType, ref sCallback))
            {
                if (sCallback.Length > iLenght)
                    sCallback = sCallback.Substring(0, iLenght);
                else
                    sCallback = sCallback.ToString();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Inquiry of system information
        /// </summary>
        /// <param name="nType">Inquired parameter type</param>
        /// <param name="pPara"> Return character information</param>
        /// <returns></returns>
        public bool CSysQuery(byte nType, ref string pPara)
        {
            pPara = "";
            IntPtr ptr = Marshal.AllocHGlobal(255);
            byte sLength = 255;
            bool bResult = RfidApi.SAAT_SysInfQuery(pHandle, nType, ptr, ref sLength);
            if (bResult)
                pPara = Marshal.PtrToStringAnsi(ptr, sLength);
            Marshal.FreeHGlobal(ptr);
            return bResult;
        }

        /// <summary>
        /// Set system information
        /// </summary>
        /// <param name="nType"></param>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public void CSysSet(byte nType, string sValue)
        {
            byte[] btValue = Encoding.GetEncoding("gb2312").GetBytes(sValue.Trim());

            if (RfidApi.SAAT_SysInfSet(pHandle, nType, btValue, (byte)(btValue.Length)))
            {
                ShowResult("Successed to Set", Win32.BeepType.Success);
            }
            else
            {
                ShowResult(CGetErrorMessage(), Win32.BeepType.Failure);
            }
        }


        /// <summary>
        /// ���ö���������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetReaderName_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtReaderName.Text.Trim()))
            {
                ShowResult("he name of reader can not be blank", Win32.BeepType.Failure); 
                return;
            }
            CSysSet(0x0, txtReaderName.Text) ;
        }

        #endregion

     

        private void btnShowCopyright_Click(object sender, EventArgs e)
        {
            StringBuilder bCopyright = new StringBuilder(); 
            RfidApi.SAAT_Copyright(out pHandle ,bCopyright);
            ShowResult(bCopyright.ToString(), Win32.BeepType.Success);
        }

        private void btnWriteCard_Click(object sender, EventArgs e)
        {
            if ((txtCardCode.Text.Length == 0) || (txtCardCode.Text.Length > 24)) { ShowResult("Tag No. can not be blank, and the length can not exceed 24bits", Win32.BeepType.Failure); txtCardCode.Focus(); return; }

            if ((txtCardCode.Text.Length % 4) != 0) { ShowResult("The entered card no. must be multiple of double bits, i.e. 4bits/8bits/12bits/16bits/20bits/24bits", Win32.BeepType.Failure); txtCardCode.Focus(); return; }

            string strTemp = "";
            string strHex = txtCardCode.Text;
            byte[] sDate = new byte[strHex.Length / 2];
            for (int i = 0; i < strHex.Length / 2; i++)
            {
                strTemp = strHex.Substring(i * 2, 2);
                sDate[i] = Convert.ToByte(strTemp, 16);
            }

            byte[] sAccessPwd = new byte[4] { 0x00, 0x00, 0x00, 0x00 };

            string sResult = CWriteEPCCode(0x00, sAccessPwd, sDate) ? "Success" : "Fail";
            ShowResult(sResult, Win32.BeepType.Success);
        }

        /// <summary>
        /// Write Epc Tag
        /// </summary>
        /// <param name="nType"></param>
        /// <param name="pAccessPWD"></param>
        /// <param name="pWriteData"></param>
        /// <returns></returns>
        public bool CWriteEPCCode(byte nType, byte[] pAccessPWD, byte[] pWriteData)
        {
            return RfidApi.SAAT_6CWriteEPCCode(pHandle, sAntenna, nType, pAccessPWD, pWriteData, (byte)pWriteData.Length);
        }

        private void btnIOOperate_Click(object sender, EventArgs e)
        {
            if (cbxPort.SelectedIndex < 0) { ShowResult("Please Select IO Port", Win32.BeepType.Failure); return; }
            if (cbxState.SelectedIndex < 0) { ShowResult("Please Select Output Value", Win32.BeepType.Failure); return; }

            string sResult = RfidApi.SAAT_IOOperate(pHandle, (byte)cbxPort.SelectedIndex, (byte)cbxState.SelectedIndex) ? "Successed" : "Failed";
            ShowResult(sResult, Win32.BeepType.Success);
            
        }



    }
}